module.exports = {
    entry: "./src/main.js",
    output: {
        path: __dirname,
        filename: "dist/bundle.js"
    },
    module: {
        loaders: [
            {test: /\.css$/, loader: "style!css"},
            {test: /\.js$/, exclude: /node_modules/, loader: 'babel?presets[]=es2015'},
            // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
            { test: /\.tsx?$/, loader: 'ts-loader' }
        ]
    },
    // Create Sourcemaps for the bundle
    devtool: 'source-map'
};
