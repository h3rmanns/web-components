/// <reference path="Action.ts" />

import Dispatcher from '../Dispatcher.ts'
import ToggleEditAction from './ToggleEditAction.ts'

export default function toggleEdit() {
    const action = new ToggleEditAction();
    Dispatcher.dispatch(action);
}
