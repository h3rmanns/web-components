var view = 'view';
var edit = 'edit';

interface Document {
    registerElement: any;
}

interface HTMLElement {
    createShadowRoot: any;
    shadowRoot: any;
}

class LvmInput extends HTMLElement {
    private mode: string;
    private text: string;
    private inputTemplate: any;
    private labelTemplate: any;
    private inputClone: any;
    private labelClone: any;
    private label: any;
    private inputEl: any;

    // life cycle method, called when an instance of the component is created
    createdCallback() {
        console.log('createdCallback');
        console.log(this instanceof LvmInput);
        this.mode = this.getAttribute('mode');
        this.text = this.getAttribute('text');
        this.render();
        this.update();
        //this.addHandlers();
    }

    render() {
        this.inputTemplate = <HTMLInputElement>document.querySelector('#input-template-input');
        this.labelTemplate = document.querySelector('#input-template-lbl');
        this.inputClone = document.importNode(this.inputTemplate.content, true);
        this.labelClone = document.importNode(this.labelTemplate.content, true);
        if (this.mode === view) {
            this.createShadowRoot().appendChild(this.labelClone);
            this.label = this.shadowRoot.getElementById('lbl');
        } else {
            this.createShadowRoot().appendChild(this.inputClone);
            this.inputEl = this.shadowRoot.getElementById("in");
        }
    }

    update() {
        if (this.inputEl) {
            this.inputEl.value = this.text;
        }
        if (this.label) {
            this.label.innerHTML = this.text;
        }

        //if (oldValue !== this.greeting) this.shadowRoot.getElementById('in').value = this.greeting;
    }


}
document.registerElement('lvm-input', LvmInput);
