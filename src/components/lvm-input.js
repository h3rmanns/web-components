var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var view = 'view';
var edit = 'edit';
var LvmInput = (function (_super) {
    __extends(LvmInput, _super);
    function LvmInput() {
        _super.apply(this, arguments);
    }
    // life cycle method, called when an instance of the component is created
    LvmInput.prototype.createdCallback = function () {
        console.log('createdCallback');
        console.log(this instanceof LvmInput);
        this.mode = this.getAttribute('mode');
        this.text = this.getAttribute('text');
        this.render();
        this.update();
        //this.addHandlers();
    };
    LvmInput.prototype.render = function () {
        this.inputTemplate = document.querySelector('#input-template-input');
        this.labelTemplate = document.querySelector('#input-template-lbl');
        this.inputClone = document.importNode(this.inputTemplate.content, true);
        this.labelClone = document.importNode(this.labelTemplate.content, true);
        if (this.mode === view) {
            this.createShadowRoot().appendChild(this.labelClone);
            this.label = this.shadowRoot.getElementById('lbl');
        }
        else {
            this.createShadowRoot().appendChild(this.inputClone);
            this.inputEl = this.shadowRoot.getElementById("in");
        }
    };
    LvmInput.prototype.update = function () {
        if (this.inputEl) {
            this.inputEl.value = this.text;
        }
        if (this.label) {
            this.label.innerHTML = this.text;
        }
        //if (oldValue !== this.greeting) this.shadowRoot.getElementById('in').value = this.greeting;
    };
    return LvmInput;
})(HTMLElement);
document.registerElement('lvm-input', LvmInput);
//# sourceMappingURL=lvm-input.js.map