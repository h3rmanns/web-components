import toggleEdit from '../actions-creators/toggle-edit.ts';

class LvmKnopf extends HTMLElement {
    createdCallback() {
        let template: any = document.querySelector('#edit-toggle');
        let clone = document.importNode(template.content, true);
        this.createShadowRoot().appendChild(clone);
        let btn: HTMLButtonElement = <HTMLButtonElement>this.shadowRoot.getElementById('btn');
        btn.addEventListener('click', (e: Event) => {
            toggleEdit();
        });
    }
}
document.registerElement('lvm-knopf', LvmKnopf);