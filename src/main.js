//require('babel/register');
//require('./greeter-component');
import './components/lvm-input.ts';
import './components/lvm-knopf.ts';
import './Dispatcher.ts';
import EditModeStore from './stores/EditModeStore.ts';

console.log(EditModeStore.editMode);

EditModeStore.register({
    stateChanged(store) {
        console.log('state changed');
        console.log(store);
        console.log(store.editMode);
    }
});

// is nich
//EditModeStore.editMode = 'edit';