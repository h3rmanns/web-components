/// <reference path="../actions-creators/Action.ts" />
/// <reference path="../components/ControllerView.ts" />

import Dispatcher from '../Dispatcher.ts'

abstract class Store {
    private views: Array<ControllerView> = [];

    constructor() {
        Dispatcher.register(this);
    }

    abstract accept(action: Action);

    register(controllerView: ControllerView) {
        this.views.push(controllerView);
    }

    protected notifyChange() {
        for (let view of this.views) {
            view.stateChanged(this);
        }
    }
}

export default Store