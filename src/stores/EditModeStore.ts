import Store from './Store.ts'
import ToggleEditAction from '../actions-creators/ToggleEditAction.ts'

class EditModeStore extends Store {
    private _editMode: string = 'view';

    accept(action: Action) {
        if (action instanceof ToggleEditAction) {
            if (this.editMode === 'view') {
                this._editMode = 'edit';
            } else {
                this._editMode = 'view';
            }
            this.notifyChange();
        }
    }

    get editMode() {
        return this._editMode;
    }
}

export default new EditModeStore();