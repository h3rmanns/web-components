/// <reference path="./actions-creators/Action.ts" />

import Store from './stores/Store.ts'

class Dispatcher {
    private stores: Array<Store> = [];

    dispatch(action: Action) {
        for (let store of this.stores) {
            store.accept(action);
        }
    }

    register(store: Store) {
        this.stores.push(store);
    }

}

export default new Dispatcher();